package headlessandgettext;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class Codehubone {

    public static void main(String[] args) throws InterruptedException {
        System.setProperty("webdriver.chrome.driver","src/main/java/chromedriver_win32/chromedriver.exe");
        ChromeOptions options=new ChromeOptions();
        options.addArguments("headless");

        WebDriver driver=new ChromeDriver(options);
        driver.navigate().to("https://www.google.com/");
        driver.findElement(By.name("q")).click();
        driver.findElement(By.name("q")).sendKeys("The statesman");
        driver.findElement(By.className("gNO89b")).click();
        Thread.sleep(3000);
        String s1=driver.findElement(By.className("LC20lb")).getText();
        System.out.println(s1);

    }
}
